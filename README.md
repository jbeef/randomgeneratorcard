# Intro
IDE: Netbeans 8.2
JDK: 1.8
SDK: Java Card 3.0.2 - Είναι μόνο για development σε Windows!

Πρόκειται για έναν servlet που έχει ένα GET operation για την δημιουργία random αριθμών (0 - 9999).
Μαζί με το Netbeans διατίθεται card emulator σαν device όπου γίνεται deploy η εφαρμογή. 
Μέσα στον emulator (smart card) σηκώνεται ένας μικρός server που ακούει στην πόρτα 8019.
Με κάθε GET request στο endpoint δημιουργείται ένας random αριθμός.

# Endpoint
http://localhost:8019/random/generate

# Tutorials
https://netbeans.org/kb/docs/javame/java-card.html
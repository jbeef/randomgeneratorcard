package eu.singularlogic.card;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Alex
 */
public class RandomGeneratorCard extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            out.write(this.generateRandom());
        } finally {
            out.close();
        }
    }
    
    private String generateRandom() {
        Random randomGenerator = new Random();
        return String.valueOf(randomGenerator.nextInt(10000));
    }
}
